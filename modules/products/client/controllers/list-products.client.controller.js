(function () {
  'use strict';

  angular
    .module('products')
    .controller('ProductsListController', ProductsListController);

  ProductsListController.$inject = ['ProductsService','$filter'];

  function ProductsListController(ProductsService,$filter) {
    var vm = this;
    vm.buildPager = buildPager;
    vm.figureOutItemsToDisplay = figureOutItemsToDisplay;
    vm.pageChanged = pageChanged;

    vm.products = ProductsService.query(function(data){
      vm.products = data;
      vm.buildPager();
    });

    function buildPager() {
      vm.pagedItems = [];
      vm.itemsPerPage = 15;
      vm.currentPage = 1;
      vm.figureOutItemsToDisplay();
    }

    function figureOutItemsToDisplay() {
      vm.filteredItems = $filter('filter')(vm.products, {
        $: vm.search
      });

      // check if filter caught other than name and SKU
      for(let i=0; i< vm.filteredItems.length; i++ ){

        if(vm.search
        && (vm.filteredItems[i].name.indexOf(vm.search) == -1
        && vm.filteredItems[i].sku.indexOf(vm.search) == -1)){
          vm.filteredItems.splice(i, 1);
          i--;
        }
      }


      vm.filterLength = vm.filteredItems.length;
      var begin = ((vm.currentPage - 1) * vm.itemsPerPage);
      var end = begin + vm.itemsPerPage;
      vm.pagedItems = vm.filteredItems.slice(begin, end);
    }

    function pageChanged() {
      vm.figureOutItemsToDisplay();
    }
  }
}());
