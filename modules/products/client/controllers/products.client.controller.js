(function () {
  'use strict';

  // Products controller
  angular
    .module('products')
    .controller('ProductsController', ProductsController);

  ProductsController.$inject = ['$scope', '$state', '$window', 'Authentication', 'productResolve','CategoriesService','Upload','Notification','$timeout','$http'];

  function ProductsController ($scope, $state, $window, Authentication, product,CategoriesService, Upload, Notification,$timeout,$http) {
    var vm = this;

    vm.authentication = Authentication;
    vm.product = product;
    vm.error = null;
    vm.form = {};
    vm.remove = remove;
    vm.save = save;
    vm.categories = CategoriesService.query();
    vm.categoryCheckbox = [];

    if(vm.product._id){
      for(let i = 0; i < vm.product.category.length;i++){
        if(vm.product.category[i].isSelected){
          vm.categoryCheckbox.push(true);
        }
        else{
          vm.categoryCheckbox.push(false);
        }
      }
    }


    // Remove existing Product
    function remove() {
      if ($window.confirm('Are you sure you want to delete?')) {
        vm.product.$remove($state.go('products.list'));
      }
    }

    // Save Product
    function save(isValid,picFile) {
      prepCategory();
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.productForm');
        return false;
      }

      // TODO: move create/update logic to service
      if (vm.product._id) {
        vm.product.$update(successCallback, errorCallback);
      } else {
        vm.product.$save(successCallback, errorCallback);
      }


      function successCallback(res) {
          prepImage(picFile);
      }

      function errorCallback(res) {
        vm.error = res.data.message;
      }

      function prepImage(blob){
        Upload.upload({
          url:'/api/picture',
          data:{
            newProductPicture: blob
          }
        }).then(function (response) {
          $timeout(function () {
            onSuccessItem(response.data);
          });
        }, function (response) {
          if (response.status > 0) onErrorItem(response.data);
        }, function (evt) {
          vm.progress = parseInt(100.0 * evt.loaded / evt.total, 10);
        });
      }
      function onSuccessItem(response) {

        vm.product.oldImagePath = vm.product.imagePath;
        vm.product.imagePath = response.imagePath;
        vm.product.isImageChange = true;
        vm.product.$update(success, errorCallback);

        // Reset form
        vm.fileSelected = false;
        vm.progress = 0;
      }
      function success(res){
        $state.go('products.view', {
          productId: product._id
        });
      }

      // Called after the user has failed to upload a new picture
      function onErrorItem(response) {
        vm.fileSelected = false;
        vm.progress = 0;

        // Show error message
        Notification.error({ message: response.message, title: '<i class="glyphicon glyphicon-remove"></i> Failed to change profile picture' });
      }

      function prepCategory(){
        vm.product.category = [];
        for(let i = 0; i < vm.categoryCheckbox.length; i++){
          if(vm.categoryCheckbox[i]){
            vm.categories[i].isSelected = true;
            vm.product.category.push(vm.categories[i]);
          }
          else{
            vm.product.category.push({isSelected: false});
          }
        }        
      }

    }
  }
}());
