'use strict';

/**
 * Module dependencies.
 */

var path = require('path'),
  fs = require('fs'),
  mongoose = require('mongoose'),
  Product = mongoose.model('Product'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  config = require(path.resolve('./config/config')),
  _ = require('lodash');

// image upload dependencies
var multer  = require('multer'),
  multerConfig = multer({ dest: './modules/products/client/imgs/' });

exports.changePicture = function(req,res){

  multerConfig.fileFilter = require(path.resolve('./config/lib/multer')).imageFileFilter;
  var upload = multer(multerConfig).single("newProductPicture");


  uploadImage()
    .then(function(){
      res.json({imagePath: "/"+req.file.path });
    });


  function uploadImage() {
    return new Promise(function (resolve, reject) {
      upload(req, res, function (uploadError) {
        if (uploadError) {
          reject(errorHandler.getErrorMessage(uploadError));
        } else {
          resolve();
        }
      });
    });
  }

  function updateProduct() {
    return new Promise(function (resolve, reject) {
      product.imagePath = '/' + req.file.path;
      product.save(function (err, product) {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      });
    });
  }
}
/**
 * Create a Product
 */
exports.create = function(req, res) {

  var product = new Product(req.body);
  product.user = req.user;

  product.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(product);
    }
  });



};



/**
 * Show the current Product
 */
exports.read = function(req, res) {

  // convert mongoose document to JSON
  var product = req.product ? req.product.toJSON() : {};

  // Add a custom field to the Article, for determining if the current User is the "owner".
  // NOTE: This field is NOT persisted to the database, since it doesn't exist in the Article model.
  product.isCurrentUserOwner = req.user && product.user && product.user._id.toString() === req.user._id.toString();

  res.jsonp(product);
};

/**
 * Update a Product
 */
exports.update = function(req, res) {

  var product = req.product;

  product = _.extend(product, req.body);
  if(product.isImageChange && product.oldImagePath !== ''){
      removeOldImage(product.oldImagePath);
  }
  product.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(product);
    }
  });

  function removeOldImage(oldImagePath){
    fs.unlink(path.resolve('.' + oldImagePath), function (unlinkError) {
      if (unlinkError) {

        // If file didn't exist, no need to reject promise
        if (unlinkError.code === 'ENOENT') {
          console.log('Removing profile image failed because file did not exist.');
          return resolve();
        }

        console.error(unlinkError);

        reject({
          message: 'Error occurred while deleting old profile picture'
        });
      } else {
        resolve();
      }
    })
  }

};

/**
 * Delete an Product
 */
exports.delete = function(req, res) {

  var product = req.product;

  product.remove(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(product);
    }
  });
};

/**
 * List of Products
 */
exports.list = function(req, res) {

  Product.find().sort('-created').populate('user', 'displayName').exec(function(err, products) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.jsonp(products);
    }
  });
};

/**
 * Product middleware
 */
exports.productByID = function(req, res, next, id) {

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Product is invalid'
    });
  }

  Product.findById(id).populate('user', 'displayName').exec(function (err, product) {
    if (err) {
      return next(err);
    } else if (!product) {
      return res.status(404).send({
        message: 'No Product with that identifier has been found'
      });
    }
    req.product = product;
    next();
  });
};
