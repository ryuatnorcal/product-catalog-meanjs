'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Product Schema
 */
var ProductSchema = new Schema({
  name: {
    type: String,
    default: '',
    required: 'Please fill Product name',
    trim: true
  },
  description: {
    type: String,
    default: '',
    required: 'Please fill Product Description',
    trim: true
  },
  picture: {
    type: Object,
    default: '',
    trim: true
  },
  imagePath: {
    type: String,
    default: '',
    trim: true
  },
  price: {
    type: Number,
    default: '',
    required: 'Please fill Product Price',
    trim: true
  },
  quantity:{
    type: Number,
    default: '',
    required: 'Please fill Product Quantity',
    trim: true
  },
  sku:{
    type: String,
    default: '',
    required: 'Please fill Product SUK',
    trim: true
  },
  category:{
    type: Object,
    default: {}
  },
  created: {
    type: Date,
    default: Date.now
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  }
});

mongoose.model('Product', ProductSchema);
